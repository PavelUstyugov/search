<?php

namespace App\Http\ApiV1\Modules\Search\Controllers;

use App\Domain\Search\Actions\SearchInElasticAction;
use App\Http\ApiV1\Modules\Search\Requests\SearchRequest;
use App\Http\ApiV1\Modules\Search\Resources\SearchResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SearchController
{
    /**
     * @param SearchRequest $request
     * @param SearchInElasticAction $action
     * @return AnonymousResourceCollection
     * @throws \Elastic\Elasticsearch\Exception\ClientResponseException
     * @throws \Elastic\Elasticsearch\Exception\ServerResponseException
     */
    public function search(SearchRequest $request, SearchInElasticAction $action): AnonymousResourceCollection
    {
        $posts = $action->execute($request->validated()['keyword']);

        return SearchResource::collection($posts);
    }
}
