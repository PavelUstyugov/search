<?php

namespace App\Http\ApiV1\Modules\Search\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SearchRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'keyword'  => ['required', 'string'],
        ];
    }
}
