<?php

namespace App\Http\ApiV1\Modules\Search\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class SearchResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text_full' => $this->text_full,
        ];
    }
}
