<?php

namespace App\Domain\Search\DTO;

use phpDocumentor\Reflection\DocBlock\Tags\Property;

/**
 * @property int $id
 * @property sting $title
 * @property string $text_full
 */
class SearchPostsDto extends \Illuminate\Support\Fluent
{

    /**
     * @param int $array
     */
    public function __construct(array $item)
    {
        parent::__construct();
        $this->id = $item['_id'];
        $this->title = $item['_source']['title'];
        $this->text_full = $item['_source']['text_full'];
    }
}