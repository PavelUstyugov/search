<?php

namespace App\Domain\Search\Actions;

use App\Domain\Search\DTO\SearchPostsDto;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Support\Fluent;
use Illuminate\Support\Collection;

class SearchInElasticAction
{
    /**
     * @var array
     */
    private array $hosts;

    /**
     * @var string
     */
    private string $index;

    /**
     * @var Client
     */
    private Client $elasticsearch;

    public function __construct()
    {
        $this->hosts = config('services.search.hosts');
        $this->index = config('services.search.index');
        $this->elasticsearch = ClientBuilder::create()->setHosts($this->hosts)
            ->build();
    }

    /**
     * @param string $keyword
     * @return Collection
     * @throws \Elastic\Elasticsearch\Exception\ClientResponseException
     * @throws \Elastic\Elasticsearch\Exception\ServerResponseException
     */
    public function execute(string $keyword): Collection
    {
        $params = [
            'index' => $this->index,
            'body' => [
                'query' => [
                    'multi_match' => [
                        'fields' => ['title^5', 'text_full'],
                        'query' => $keyword,
                    ],
                ],
            ],
        ];
        $response = $this->elasticsearch->search($params);

        $items = array_map(function ($array){
            return   new SearchPostsDto($array);
        }, $response['hits']['hits']);

        return collect($items);

    }
}
